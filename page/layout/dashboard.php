<link rel="stylesheet" href="<?=asset('datatable/datatable.css')?>">

<script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>

<!-- Dropdown Structure -->
    <!-- Dropdown Master Data -->
    <ul id="dropdown_master_data" class="dropdown-content">
        <li><a href="<?=url('/mahasiswas')?>">Mahasiswa</a></li>
        <li><a href="<?=url('/data_trainings')?>">Data Training</a></li>
    </ul>
<nav>
    <div class="nav-wrapper">
        <ul class="left hide-on-med-and-down">
            <li><a href="<?=url('/')?>"><i class="material-icons left">home</i> Home</a></li>
            <li><a class="dropdown-trigger" href="#!" data-target="dropdown_master_data"><i class="material-icons left">list</i> Master Data <i class="material-icons right">arrow_drop_down</i></a></li>
        </ul>
    </div>
</nav>

<!-- Start Content -->
<br/>
<div style="width:95%; margin:0 auto;">
    <?php
        include load_page($page);
    ?>
</div>


<!-- End Content -->

<script src="<?=asset('datatable/datatable.js')?>"></script>