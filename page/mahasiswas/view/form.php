<?php
    $penghasilan_ayah = [
        "0" => "Tidak Berpenghasilan",
        "1" => "< 500.000",
        "2" => "500.000 - 1.000.000",
        "3" => "1.000.000 - 2.000.000",
        "4" => "> 2.000.000",
    ];

    $penghasilan_ibu = [
        "0" => "Tidak Berpenghasilan",
        "1" => "< 500.000",
        "2" => "500.000 - 1.000.000",
        "3" => "1.000.000 - 2.000.000",
        "4" => "> 2.000.000",
    ];

    $jenis_transportasi = [
        "1" => "Jalan Kaki",
        "2" => "Sepeda",
        "3" => "Sepeda Motor",
        "4" => "Mobil",
    ];

    $k = [1=>1,3=>3,5=>5];
?>
<form class="card z-depth-3" action="<?=(($mahasiswa ?? '') != '') ? url('/mahasiswas/update', $mahasiswa['mahasiswa_id']) : url('/mahasiswas/store')?>" method="post">
    <input type="hidden" name="model" value=mahasiswas id="model"/>
    <div class="card-content row">
        <div class="col s12 m8 offset-m2">
    <?php
        if (($mahasiswa['result'] ?? false) ? true : false)
        {
    ?>
            <div class="center-align">
                <?= ($mahasiswa['result'] == 'Layak') ? '<span class="badge green white-text">'.$mahasiswa['result'].'</span>' : '<span class="badge red white-text">'.$mahasiswa['result'].'</span>' ?>
            </div>
    <?php
        }
    ?>
            <div class="input-field" style="display:none;">
                <input name="mahasiswa_id" id="mahasiswa_id" value="<?= $mahasiswa['mahasiswa_id'] ?? ''?>" type="text" class="validate" readonly>
                <label for="mahasiswa_id">Mahasiswa ID</label>
            </div>
            <div class="input-field">
                <input name="name" id="name" value="<?= $mahasiswa['name'] ?? ''?>" type="text" class="validate" required autofocus>
                <label for="name">Nama Mahasiswa</label>
            </div>
            <div class="input-field">
                <select name="penghasilan_ayah" id="penghasilan_ayah" class="validate" required>
            <?php
                foreach ($penghasilan_ayah as $key => $value) {
            ?>
                    <option value="<?=$key?>" <?=(($mahasiswa['penghasilan_ayah'] ?? '') == $key) ? 'selected' : ''?>><?=$value?></option>
            <?php
                }
            ?>
                </select>
                <label for="penghasilan_ayah">Penghasilan Ayah</label>
            </div>
            <div class="input-field">
                <select name="penghasilan_ibu" id="penghasilan_ibu" class="validate" required>
            <?php
                foreach ($penghasilan_ibu as $key => $value) {
            ?>
                    <option value="<?=$key?>" <?=(($mahasiswa['penghasilan_ibu'] ?? '') == $key) ? 'selected' : ''?>><?=$value?></option>
            <?php
                }
            ?>
                </select>
                <label for="penghasilan_ibu">Penghasilan Ibu</label>
            </div>
            <div class="input-field">
                <p>
                <label>
                    <input type="checkbox" name="penerima_kps" class="filled-in" <?= (($mahasiswa['penerima_kps'] ?? false)) ? 'checked' : '' ?>/>
                    <span>Penerima KPS</span>
                </label>
                &nbsp;&nbsp;&nbsp;
                <label>
                    <input type="checkbox" name="penerima_kip" class="filled-in" <?= (($mahasiswa['penerima_kip'] ?? false)) ? 'checked' : '' ?>/>
                    <span>Penerima KIP</span>
                </label>
                </p>
            </div>
            <div class="input-field">
                <select name="jenis_transportasi" id="jenis_transportasi" class="validate" required>
            <?php
                foreach ($jenis_transportasi as $key => $value) {
            ?>
                    <option value="<?=$key?>" <?=(($mahasiswa['jenis_transportasi'] ?? '') == $key) ? 'selected' : ''?>><?=$value?></option>
            <?php
                }
            ?>
                </select>
                <label for="jenis_transportasi">Jenis Transportasi</label>
            </div>
            <div class="input-field">
                <select name="k" id="k" class="validate" required>
            <?php
                foreach ($k as $key => $value) {
            ?>
                    <option value="<?=$key?>" <?=(($mahasiswa['k'] ?? '') == $key) ? 'selected' : ((($key) == 3) ? 'selected' : '')?>><?=$value?></option>
            <?php
                }
            ?>
                </select>
                <label for="k">K</label>
            </div>
        </div>
    </div>
    <div class="card-action row">
        <div class="col s12 m8 offset-2">
            <button id="button-submit" type="submit" name="<?=(($mahasiswa ?? '') != '') ? 'update' : 'store'?>" class="btn green"><?=(($mahasiswa ?? '') != '') ? 'Update' : 'Save'?></button>
        </div>
    </div>
</form>