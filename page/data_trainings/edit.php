<?php
    $model_data_training = new DataTraining();
    $data_training = $model_data_training->find($_GET['id']);

    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Data Training',
            'link' => url('/data_trainings')
        ],
        [
            'title' => $data_training['name'],
            'link' => 'javascript:void(0)'
        ],
    ];

    include_once load_component('breadcrumb');
?>
<br>
<?php
    include 'view/form.php';
    
?>