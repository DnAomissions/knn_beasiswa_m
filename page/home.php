<?php
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => 'javascript:void(0)'
        ]
    ];

    include_once ROOT.'/page/components/breadcrumb.php';

?>
<div class="card center-align z-depth-2" style="margin-top: 15px;">
    <div class="card-content">
        <span class="card-title">Welcome to KNN Application!</span>
    </div>
    <div class="card-action">
        <a href="<?=url('/mahasiswas')?>" class="btn green">Data Mahasiswa</a> | <a href="<?=url('/data_trainings')?>" class="btn orange">Data Training</a>
    </div>
</div>