<?php
$model = new DataTraining();

$request = getRequest();

if(isset($request['penerima_kps']))
{
    $request = arrayUpdate($request, 'penerima_kps', true);
}else{
    $request = arrayUpdate($request, 'penerima_kps', 0);
}

if(isset($request['penerima_kip']))
{
    $request = arrayUpdate($request, 'penerima_kip', true);
}else{
    $request = arrayUpdate($request, 'penerima_kip', 0);
}


if(isset($_GET['export']))
{
    $export = $model->export();
    
    if($export)
    {
        $session->setSession('success', 'Success export Data Training!');
    }else{
        $session->setSession('error', 'Failed export Data Training!');
    }
}

if(isset($_POST['import']))
{
    $file = $_FILES['file_import'];

    $upload = fileUpload($file);

    if(!$upload)
    {
        $session->setSession('error', 'Failed import Data Training!');
        echo "<script>window.location.replace('".url('/data_trainings')."')</script>";
        exit;
    }

    $import = $model->import($upload['dirname'].'/'.$upload['basename'], $upload['extension']);
    
    if(!empty($import))
    {
        $session->setSession('success', 'Success import Data Training!');
    }
}

if(isset($_POST['store']))
{
    $model = $model->create($request);

    if(!empty($model))
    {
        $session->setSession('success', 'Success create New Data Training!');
    }
}

if(isset($_POST['destroy']))
{
    if(!isset($_POST['data_training_id'])){
        $session->setSession('warning', 'Data Training ID not identified!');
    }else{
        if($model->delete($_POST['data_training_id']))
        {
            $session->setSession('success', 'Success delete Data Training!');
        }else{
            $session->setSession('warning', 'Failed delete Data Training!');
        }
    }
}

if(isset($_POST['update']))
{
    if(!isset($_POST['data_training_id'])){
        $session->setSession('warning', 'Data Training ID not identified!');
    }else{
        $model = $model->update($_POST['data_training_id'], $request);

        if(!empty($model))
        {
            $session->setSession('success', 'Success edit Data Training!');
        }else{
            $session->setSession('warning', 'Failed edit Data Training!');
        }
    }
}

echo "<script>window.location.replace('".url('/data_trainings')."')</script>";
exit;

?>