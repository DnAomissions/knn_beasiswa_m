<?php
$model = new Mahasiswa();

$request = getRequest();

if(isset($request['penerima_kps']))
{
    $request = arrayUpdate($request, 'penerima_kps', true);
}else{
    $request = arrayUpdate($request, 'penerima_kps', 0);
}

if(isset($request['penerima_kip']))
{
    $request = arrayUpdate($request, 'penerima_kip', true);
}else{
    $request = arrayUpdate($request, 'penerima_kip', 0);
}


if(isset($_GET['proses_data']))
{
    if(!isset($_GET['id'])){
        $session->setSession('warning', 'Mahasiswa ID not identified!');
    }else{
        $model = $model->update($_GET['id'], [
            'result' => $model->getResult($_GET['id']),
            'sudah_diuji' => 1
        ]);

        if(!empty($model))
        {
            if($model['result'] == 'Layak')
            {
                $session->setSession('success', $model['name'].' dinyatakan '.$model['result'].'!');
            }else{
                $session->setSession('error', $model['name'].' dinyatakan '.$model['result'].'!');
            }
        }else{
            $session->setSession('warning', 'Failed proses data Mahasiswa!');
        }
        echo "<script>window.location.replace('".url('/mahasiswas/edit', $_GET['id'])."')</script>";
        exit;
    }
}

/**
 * Basic Method
 */

if(isset($_GET['export']))
{
    $export = $model->export();
    
    if($export)
    {
        $session->setSession('success', 'Success export Mahasiswa!');
    }else{
        $session->setSession('error', 'Failed export Mahasiswa!');
    }
}

if(isset($_POST['import']))
{
    $file = $_FILES['file_import'];

    $upload = fileUpload($file);

    if(!$upload)
    {
        $session->setSession('error', 'Failed import Mahasiswa!');
        echo "<script>window.location.replace('".url('/mahasiswas')."')</script>";
        exit;
    }

    $import = $model->import($upload['dirname'].'/'.$upload['basename'], $upload['extension']);
    
    if(!empty($import))
    {
        $session->setSession('success', 'Success import Mahasiswa!');
    }
}

if(isset($_POST['store']))
{
    $model = $model->create($request);

    if(!empty($model))
    {
        $session->setSession('success', 'Success create New Mahasiswa!');
    }
}

if(isset($_POST['destroy']))
{
    if(!isset($_POST['mahasiswa_id'])){
        $session->setSession('warning', 'Mahasiswa ID not identified!');
    }else{
        if($model->delete($_POST['mahasiswa_id']))
        {
            $session->setSession('success', 'Success delete Mahasiswa!');
        }else{
            $session->setSession('warning', 'Failed delete Mahasiswa!');
        }
    }
}

if(isset($_POST['update']))
{
    if(!isset($_POST['mahasiswa_id'])){
        $session->setSession('warning', 'Mahasiswa ID not identified!');
    }else{
        $model = $model->update($_POST['mahasiswa_id'], $request);

        if(!empty($model))
        {
            $session->setSession('success', 'Success edit Mahasiswa!');
        }else{
            $session->setSession('warning', 'Failed edit Mahasiswa!');
        }
    }
}

echo "<script>window.location.replace('".url('/mahasiswas')."')</script>";
exit;

?>